\documentclass[10pt, twoside, a4paper]{article}
\usepackage[latin1]{inputenc}
\usepackage[bottom=1.5cm, right=3.2cm, top=2.5cm, left=3.2cm]{geometry}
\usepackage{fancyhdr}
\usepackage[leqno]{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{indentfirst}
\usepackage{titlesec}
\usepackage{enumitem}
\usepackage{mathtools}
\usepackage[none]{hyphenat}
\usepackage{hyperref}
\usepackage{pgfplots, pgfplotstable}
\usepackage{tikz}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{polar}
\newtheoremstyle{thstyle}{3pt}{3pt}{\itshape}{}{\bfseries}{.}{.5em}{}
\titlelabel{\texttt\thetitle.\hskip1ex\relax}
\titleformat{\section}{\normalfont\centering\scshape}{\thesection.}{0.5em}{}
\fancyhf{}
\fancyhead[CE]{\small N.S. PAPAGEORGIOU, V. R\u{A}DULESCU, AND DUSAN REPOVS}%
\fancyhead[CO]{\small PERIODIC SOLUTIONS FOR IMPLICIT EVOLUTION INCLUSIONS}%
\fancyhead[LE,RO]{\small\thepage}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}

\theoremstyle{thstyle}
\newtheorem{definition}{Definition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{theorem}[definition]{Theorem}
\newtheorem{remark}{Remark}

\makeatletter
\def\keywords{\xdef\@thefnmark{}\@footnotetext}
\makeatother
\renewcommand{\qedsymbol}{}

\begin{document}
%====================================== PAGE 1 ===========================================
\title{\bfseries\large PERIODIC SOLUTIONS FOR IMPLICIT EVOLUTION INCLUSIONS}
\author{\small NIKOLAOS S. PAPAGEORGIU, VICEN\c{T}IU D. R\u{A}DULESCU, AND DUSAN REPOVS}
\date{}
\maketitle\label{title}
\begin{center}\begin{flushleft}
	\textsc{Abstract.} We consider a nonlinear implicitly evolution inclusion driven by a nonlinear, nonmonotone, time-varying set-valued map and defined in the framework of an evolution triple of Hilbert spaces. Using an approximation technique and surjectivity result for parabolic operators of monotone-type, we show the existence of a periodic solution.\end{flushleft}
\end{center}
\section{Introduction}
In this paper we study the following periodic implicit evolution inclusion
\begin{equation}
\left\{\begin{array}{lr}\frac{d}{dt}(Bu(t))+A(t,u(t))\ni0\text{ for almost all } t\in T=[0,b] \\
B(u(0)) = B(u(b))\end{array}\right\}	
\end{equation}

Problem (1) is defined in the framework of an evolution triple $(X,H,X^{*})$ of Hilbert spaces (see Section 2), with $B \in \mathcal{L} (X,X^{*}) \text{ and } A : T \times X \rightarrow 2^{X^{*}}$ is a map measurable in $t /in T$ and for almost all $t \in T A(t,\cdot)$ is bounded and pseudomonotone.

Implicit evolution equations were studied by Andrews-Kuttler-Schillor [1], Barbu [2], Barbu-Favini [3], Favini-Yagi [6], Liu [11], Showalter [14]. However, in all these works A is time-invariant and maximal monotone. Moreover, the aforementioned works treat the Cauchy problem. We are not aware of any work on implicit evolution equations treating the periodic problem. We mention also the works of Barbu-Favini [4] and DiBenedetto- Showalter [5], treating the case where B is nonlinear monotone. For this case the hypotheses and the techniques are different.
\section{Mathematical Background}
Suppose that $X$ and $Y$ are Banach spaces and $X$ is embedded continuously and densely into $Y$ . Then we know that $Y^{*}$ is embeded continuously into $X^{*}$ and if $X$ is reflexive, then the embedding of $Y^{*}$ into $X^{*}$ is also dense.
\begin{definition}
	By an evolution triple, we mean a triple of spaces
\[
	X \hookrightarrow H \hookrightarrow X^{*}
\]
such that $X$ is a separable reflexive Banach space, $H$ is a separable Hilbert space identified with its dual (pivot space) and $X$ is embedded continuously into $H$. We say that $(X,H,X^{*})$ is an evolution triple of Hilbert spaces, if all three spaces are Hilbert.
\end{definition}

Evidently $H^{*}=H$ is an embedded continuously and densely into $X^{*}$. By $||\cdot||$(resp$|\cdot|$,$||\cdot||_{*}$), we denoate the norm of $X$ (resp. of $H$, $X^{*}$). We have
\[
	|\cdot| \leqslant c_1||\cdot||_{*}\leqslant c_2|\cdot|\text{ for some } c_1,c_2\:> \: 0
\]

By $\langle\cdot,\cdot\rangle$ we denote the duality brackets for the pair $(X^{*},X)$ and by $(\cdot,\cdot)$ the inner product of $H$. We have
\[
	\langle\cdot,\cdot\rangle|_{H\times X}=(\cdot,\cdot)
\]

Given an evolution triple $(X,H,X^{*})$ and $1<p<\infty$, we can define the following Banach space
\keywords{\emph{Key words and phrases.} Evolution triple, compact embedding, pseudomonotone map, coercive map, implicit inclusion, periodic solutions}
\keywords{2010 AMS Subject Classification: 34A20, 35A05, 35R70}
\pagebreak
%===================================== PAGE 2 =====================================================
\newpage
\[
	W_{p}(0,b) = \{u\in L^{p}(T,X):u'\in L^{p'}(T,X^{*}\}
\]

In this definition $1<p<\infty,\frac{1}{p}+\frac{1}{p'}=1$ and the derivative of $u'$ is understood in the sense of vectorial distributions. A function $u\in W_{p}(0,b)\subseteq AC^{1,p'}(T,X^{*})=W^{1,p'}((0,b),X^{*})$. Also, we know that $L^{p}(T,X^{*})=L^{p'}(T,X^{*})$. The space $W_{p}(0,b)$ is embedded continuously and densely into $C(T,H)$ and its elements satisfy the following integration by parts formula
\begin{proposition}
	If $(X,H,X^{*})$ is an evolution triple and $u,v\in W_{p}(0,b)(1<p<\infty)$, then $t\rightarrow(u(t),v(t))$ is absolutely continuous and
	\[
		\frac{d}{dt}(u(t),v(t))=(u'(t),v(t))+(u(t),v'(t))\text{ for almost all } t\in T
	\]
\end{proposition}

If $(X,H,X^{*}$ is an evolution triple and $X$ is embedded compactly into $H$, then $H^{*} = H$ is embedded $X^{*}$ (Schauder's theorem) and $W_{p}(0,b)$ is embedded compactly into $L^{p}(T,H)$. For details see Gasinski-Papageorgiou [7].

We will use the following notions from multivalued analysis (see [9])
\begin{enumerate}[label=(\alph*)]

\item If $V,W$ are Hausdorff topological spaces and $G:V\rightarrow 2^{W}\setminus\{\varnothing\}$ then we say that $\{v\in V:G(v)\bigcap C\neq \varnothing\}$ is closed.
\item If $T=[0,b], Y$ is a separable Banach space and $G:T\rightarrow 2^{Y}\setminus\{\varnothing\}$, then we say that $G(\cdot)$ is a graph measurable if
\[
	GrG=\{(t,y)\in T\times Y:y\in G(t)\}\in \mathcal{L}_{T} \oplus B(Y),
\]
with $\mathcal{L}_{T}$ being the Legesgue $\sigma$ -field on $Y$.
\end{enumerate}
Given a Banach space, we will use the following notation
\[
	P_{f(c)}(X)=\{C \subseteq Y : C \text{ is nonempty, closed (and conved)}\}
\]
Also, if $C\subseteq Y$, then we define
\[
	|C|=\sup{[||c||]_{y}:c \in C}.
\]

Let $Y$ be a reflexive Banach space and $A:Y\rightarrow 2^{Y^{*}}.$ We say that $A(\cdot)$ is "pseudomonotone", if the following conditions are satisified:
\begin{itemize}
	\item For every $y\in Y A(y)$ is nonempty, closed, conved
	\item $A(\cdot)$ is bounded (that is, maps bounded sets to bounded sets)
	\item If $y_{n}\xrightarrow[]{w}y$ in $Y,y^{*}_{n}\xrightarrow[]{w}y^{*}$ in $Y^{*}$ with $y^{*}_{n}\in A(y_{n}$ for all $n\in \mathbb{N}$ and
\[
	\limsup\limits_{n\rightarrow\infty}\langle y^{*}_{n},y_{n}-y\rangle_{Y*Y}\leqslant 0,
\]
then $y^{*}\in A(y)$ and $\langle y^{*}_{n},y_{n}\rangle_{Y*Y}\rightarrow \langle y^{*}, y\rangle_{Y*Y}.$
\end{itemize}

A maximal monotone map $A:Y\rightarrow 2^{Y^{*}}\setminus\{\varnothing\}$ is pseudomonotone (see Gasinski-Papageorgiou [7], pp. 331-331). As in the case with the maximal monotone maps, pseudomonotone operators exhibit nice surjectivity properties. In particular a pseudomonotone coercive (that is, $\frac{\inf[\langle y^{*},y\rangle_{Y*Y}:y^{*}\in A(y)]}{||y||_{Y}}\rightarrow +\infty$ as $||y||_{Y}\rightarrow +\infty$) map is surjective (see Gasinski-Papageorgiou [7], p. 326).

For dynamic problems (evolution equations), we have the following variants of the notion of pseudomonotonicity.
\begin{definition}
	Let $Y$ be a reflextive Banach space, $L:D(L)\subseteq Y^{*}$ is a linear, maximal monotone operator and $A:Y\rightarrow 2^{Y*}$.We say that $A(\cdot)$ is a "L-pseudomonotone", if the following conditions hold.
\end{definition}
	\begin{enumerate}[label*=(\roman*)]
		\item For every $y\in Y, A(y)\subseteq Y^{*}$ is nonempty, W-compact, convex.
		\item The multifunction $A:Y\rightarrow 2^{Y*}\setminus\{\varnothing\}$ is use from every finite dimensional subspace of $Y$ into $Y^{*}$ furnished with the weak topology.
		\item If $\{y_{n}\}_{n\geqslant 1}\subseteq D(L), y_{n}\xrightarrow[]{w}y\in D(L)\text{ in } Y,L(y)\xrightarrow[]{w} L(y)\text{ in }Y^{*},y^{*}_{n}\in A(y)_{n}\text{ for all }n\in n\in\mathbb{N}, y^{*}_{n}\xrightarrow[]{w}y^{*}\text{ in }Y^{*}\text{ and }\limsup\limits_{n\rightarrow\infty}\langle y^{*}_{n},y_{n}-y\rangle\leqslant0,\text{ then } y^{*}\in A(y)\text{ and }\langle y^{*}_{n},y_{n}\rangle_{Y*Y}\rightarrow\langle y^{*},y\rangle_{Y*Y}$
	\end{enumerate}
\pagebreak
% ====================================== PAGE 3 ==================================

These operators too have nice surjectivity properties. The next result can be found in Papageorgiou-Papalini-Renzacci [12] (the single valued version of it, is due to Lions [10])
\begin{theorem}
	If $Y$ is a reflexive Banach space which is strictly convex, $L:D(L)\subseteq Y\rightarrow Y^{*}$ is a linear, maximal monotone operator and $A:Y\rightarrow 2^{Y*}$ is bounded, L-pseudomonotone, coercive, then $L+A$ is surjective.
\end{theorem}
\section{Periodic Solutions}
In what follows $T=[0,b]$ and $(X,H,X^{*})$ is an evolution triple of Hilbert spaces. WE assume that X is embedded compactly into $H$ (hence so does $H^{*} = H$ into $X^{*}$). The hypotheses on the data of (1) are the following:

	$H(B): B\in\mathcal{L}(X,X^{*})$ and it is symmetric and monotone
	
	$H(A): A:T\times X\rightarrow P_{f_{c}}(X^{*})$ is a multivalued map such that
\begin{enumerate}[label*=(\roman*)]
	\item for all $x\in X,t\rightarrow A(t,x)$ is graph measurable
	\item for almost all $t\in T,x\rightarrow A(t,x)$ is pseudomonotone
	\item for almost all $t\in T$ and all $x\in X$, we have
\[
	|A(t,x)|\leqslant c_1(t)+c_2||x||^(p-1)
\]
with $c_1\in L^{p'},2\leqslant p < \infty \text{ and } c_2 > 0$
	\item for almost all $t\in T$ and all $x\in X$, we have
\[
	\inf[\langle u^{*},x\rangle :u^{*}\in A(t,x)]\geqslant c_3||x||^{p}-c_4(t) \text{ with
 } c_3 > 0 \text{ and } c_4\in L'(T).
\]
\end{enumerate}

Let $J:X\rightarrow X^{*}$ be the duality (Riesz) map for the Hilbert spaces $X$. We know that $J(\cdot)$ is an isometric isomorphism (Riesz-Frechet theorem) which is monotone. Hence for every $\epsilon > 0 (\epsilon J+B)^{-1}\in\mathcal{L}(X^{*},X)$. Then on a $X^{*}$ we consider the following bilinear from
\begin{equation}
	(u,v)_{*}=\langle(\epsilon J+B)^{-1}u,v\rangle\text{ for all }u,v\in X^{*}
\end{equation}

Hypotheses $H(B)$ imply that $(\cdot,\cdot)_{*}$ is an inner product on $X^{*}$. Let $|\cdot|_{*}$ denote the norm corresponding to this inner product. Clearly $|\cdot|_{*}$ and $||\cdot||_{*}$ are equivalent norms on $X^{*}$. So, if $V^{*}$ denotes the space $X^{*}$ equipped with the norm $|\cdot|$, then $V^{*}$ is a Hilbert space. Using the Reisz-Frechet theorem we identify $V^{*}$ with it's dual.

Let $A_{\epsilon}:T\times V^{*}\rightarrow P_{f_{c}}(V^{*})$ be defined by
\[
	A_{\epsilon}(t,v)=A(t,(\epsilon J+B)^{-1}v).
\]

Then we introduce $\hat{A}_{\epsilon}:L^{p}(T,V^{*})\rightarrow 2^{L^{p'}(T,V^{*})}$ the multivalued Nemitsky map corresponding to $\hat{A}(\cdot,\cdot)$ defined by
\[
	\hat{A}_{\epsilon}(v)=\{u\in L^{p'}(T,V^{*}):u(t)\in A_{\epsilon}(t,v(t))\text{ for almost all }t\in T\}
\]

Also let $W^{per}_{p}((0,b),V^{*})=\{u\in L^{p}(T,V^{*}):u'\in L^{p'},u(0)=u(b)\}$. We know that $W^{per}_{p}((0,b), V^{*})\hookrightarrow C(T,V^{*})$ and so the evaluations of $u$ at $t=0$ and at $t=b$ make sense.
Let $L:W^{par}_{p}((0,b),V^{*})\subseteq L^{p}(T,V^{*})\rightarrow L^{p'}(T,V^{*})$ be defined by
\[
	L(u)=u'
\]

We know that $L(\cdot)$ is linear, maximal monotone (see Hu-Papageorgiou [9] (p. 419) and Zeidler [15] (p. 855)).
\begin{proposition}
	If hypotheses $H(B),H(A)$ hold and $\epsilon > 0$, then for every $u\in L^{p}(T,V^{*}\hat{A}_{\epsilon}(u)\subseteq L^{p'}(T,V^{*})$ is nonempty, w-compact, convex and $u\rightarrow \hat{A}_{\epsilon}$ is L-pseudomonotone.
\end{proposition}
\begin{proof}
	It is clear that $\hat{A}_{\epsilon}$ is closed, conved, bounded, thus $w$-compact in $L^{p'}(T,V^{*}).$ We need to show that $\hat{A}_{\epsilon}(\cdot)$ has nonempty values. Note that hypotheses $H(A)$($i$),($ii$) do not imply the graph measurability of $(t,x)\rightarrow A_{\epsilon}$ (see Hu-Papageorgiou [9], p. 227). To show the nonemptiness of $\hat{A}_{\epsilon}(u)$ we proceed as follows. Let $\{s_{n}\}_{n\geqslant} \subseteq L^{p}(T,V^{*})$ be step functions such that
\begin{align*}
	s_{n}\rightarrow u \text{ in } L^{p}(T,V^{*}),s_{n}(t)\rightarrow u(t)\text{ for almost all } t\in T,\\
	\quad |s_{n}(t)|_{*}\leqslant|u(t)|_{*}\text{ for almost all }t\in T,\text{ all }n\in\mathbb{N}.
\end{align*}
\end{proof}
% ===================================== PAGE 4 =====================================
On account of hypothesis H(A)($i$) for every $n\in \mathbb{N},t\rightarrow A_{\epsilon}(t,s_{n}(t))=A(t,(\epsilon J+B)^{-1}s_{n}(t))$ is graph measurable. So, we can apply the Yahkov-von Newumann-Aumann selection theorem (see Hu-Papageorgiou [9], p. 158) and obtain $v_{n}:T\rightarrow V^{*}$ measurable such that $v_{n}(t)\in A_{\epsilon}(t,s_{n}(t))$ for almost all $t\in T,n\in\mathbb{N}$. Evidently $v_{n}\in L^{p'}(T,V^{*}$ and $\{v_{n}\}_{n\geqslant1}\subseteq L^{p'}(T,V^{*})$ is bounded. So, by passing to a suitable subsequence if necessary we may assume that
\begin{equation}
	v_{n}\xrightarrow[]{w} v\text{ in } L^{p'}(T,V^{*}\text{ as }n\in\infty. 
\end{equation}
Note that the pseudomonotonicity of $A_{\epsilon}(t,\cdot)$ (see hypothesis H(A)($ii$)) implies that $GrA_{\epsilon}(t,\cdot)$. is demiclosed (that is, sequentially closed in $V^{*}\times V^{*}_{W}$ denotes the Hilbert space $V^{*}$ furnished with weak topology). So, from (3) and Proposition 3.9, p. 694, of Hu-Papageorgiou [9], we have
\begin{alignat*}{2}
  &&v(t)\in \overline{conv}w
  & \-\limsup\limits_{n\rightarrow\infty}A_{\epsilon}(t,s_{n}(t))\subseteq A_{\epsilon}(t,u(t))\text{ for almost all }t\in T,\\
  &\Rightarrow\qquad
  &v(t)
  &= \hat{A}_{\epsilon}(u)\text{ and so }\hat{A}_{\epsilon}(\cdot)\text{ has nonempty values.}\\
\end{alignat*}

Next we will show the L-psseudomonotonoicity of $\hat{A}_{\epsilon}$. So, let $((\cdot,\cdot))_{*}$ denote the duality brackets for the pair $(L^{p'}(T,V^{*}),L^{p}(T,V^{*}))$, that is,
\begin{equation}
	((v,u))_{*}=\int_{0}^{b}(v(t),u(t))_{*}dt\text{ for all }u\in L^{p}(T,V^{*}),v\in L^{p'}(T,V^{*})
\end{equation}

Consider a sequence $\{u_{n}\}_{n\geqslant 1}\subseteq W^{per}_{p}((0,b),V^{*})$ such that
\begin{align}
	"u_{n}\xrightarrow[]{w}u L^{p}(T,V^{*}),u'_{n}\xrightarrow[]{w}u'\text{ in } L^{p'}(T,V^{*}) \text{ and }v_{n}\in\hat{A}_{\epsilon}(u_{n})n\in\mathbb{N},[\notag\\\text{ such that } v_{n}\xrightarrow[]{w}v in L^{p'}(T,V^{*})\text{ and }\limsup\limits_{n\rightarrow\infty}((v_{n},u_{n}-u))_{*} \leqslant 0"
\end{align}

We have
\begin{align*}
	((v_{n},u_{n}-u))_{*} & =\int_{0}^{b}(v_{n}(t),u_{n}(t)-u(t))_{*}dt\text{ (see (4))}\\
& =\int_{0}^{b}\langle v_{n}(t),(\epsilon J+B)^{-1}(u_{n}-u)(t)\rangle dt\text{ (see (2)).}
\end{align*}

Let $y_{n}(t)=(\epsilon J+B)^{-1}u_{n}(t),y(t)=(\epsilon J+B)^{-1}u(t)$. Then $y_{n},y\in L^{p}(T,X)$ and we have 
\[
	\langle v_{n}(t),(\epsilon J+B)^{-1}(u_{n}-u)(t)\rangle=\langle v_{n}(t),y_{n}(t)-y(t))
\]
with $v_{n}(t)\in A(t,y_{n}(t))$ for almost all $t\in T$, all $n\in\mathbb{N}$. Evidently
\begin{equation}
	\{y_{n}\}_{n\geqslant 1}\subseteq L^{p}(T,X)\text{ is bounded. (see (5)).}
\end{equation}

Also, we have
\begin{align}
	&y'_{n}=((\epsilon J+B)^{-1}u_{n})'\notag\\
	\Rightarrow\quad&\{y'_{n}\}_{n\geqslant 1}\subseteq L^{p'}(T,X^{*})\text{ is bounded (see (5))}
\end{align}

From (6) and (7) it follows that
\[
	\{y_{n}\}_{n\geqslant 1}\subseteq W_{p}(0,b)\text{ is bounded.}
\]

So we may assume that
\begin{equation}
	y_{n}\xrightarrow[]{w}y\text{ in }W_{p}(0,b)\text{ as }n\rightarrow\infty
\end{equation}

Evidently we have $y=(\epsilon J+B)^{-1}u$ and so
\[
	(\epsilon J+B)^{-1}u_{n}\xrightarrow[]{w}(\epsilon J+B)^{-1}\text{ in }L^{p}(T,X).
\]

If by $((\cdot,\cdot))$ we denote the duality brackets for the pair $(L^{p'}(T,X^{*}),L^{p}(T,X))$ that is
\[
	((v,u))=\inf^{b}_{0}\langle v(t),u(t)\rangle dt\text{ for all }u\in L^{p}(T,X),v\in L^{p'}(T,X^{*}),
\]
then we have
\[
	\limsup\limits_{n\rightarrow\infty}((v_{n},y_{n}-y))=\limsup\limits_{n\rightarrow\infty}((v_{n},u_{n}-u))\leqslant 0\text{(see (5))}
\]

Recall that $W_{p}(0,b)$ is embedded continuously in $C(T,H)$. So, from (8) we have
\begin{equation}
	y_{n}(t)\xrightarrow[]{w}y(t)\text{ in }H\text{ for all }t\in T.
\end{equation}

Let $\vartheta_{n}(t)=\langle v_{n}(t),y_{n}(t)-y(t))$ and let $N \subseteq T$ be the Lebesgue-null set outside of which hypotheses $H(A)(ii)(iii)(iv)$ hold. Then for the $t\in T\setminus N$, we have
\begin{align}
	V_{n}(t)\geqslant c_3||y_{n}(t)||^{p}-c_4(t)&-||y(t)||[c_1(t)+c_2||y_{n}(t)||^{p-1}]\\
	&\text{see hypotheses H(A)(iii)(iv)}\notag
\end{align}

Let $E=\{t\in T:\liminf\limits_{n\rightarrow\infty}\vartheta_{n}(t)<0\}$. This is a Lebesgue measurable set. Suppose that $\lambda^{1}(E)>0(\lambda^{1}(\cdot)$ denotes the Lebesgue measure on $\mathbb{R}$). From (10), we see that $\{y_{n}(t)\}_{n\geqslant 1}\subseteq X$ is bounded for all $t\in E\cap(T\setminus N)$. So, on account of (9) we will have $y_{n}(t)\xrightarrow[]{w}y(t)$ in $X$. Fix $t\in E\cap (T\setminus N)$ and choose a suitable subsequence (depending on $t$) such that $\liminf\limits_{n\rightarrow\infty}\vartheta_{n}(t)=\lim\limits_{k\rightarrow\infty}\vartheta_{n_{k}}(t)$. The pseudomenotonicity of $A(t,\cdot)$ (see hypothesis H(A)($ii$)), implies that
\[
	\langle v_{n_{k}}(t),y_{n_{k}}(t)-y(t)\rangle\rightarrow 0,
\]
a contradiction since $t\in E$. Therefore $\lambda^{1}(E)=0$ and so we have
\begin{equation}
	0 \leqslant \liminf\limits_{n\rightarrow\infty}\vartheta_{n}(t)\text{ for almost all }t\in T.
\end{equation}

Invoking Fatou's lemma, we have
\begin{align}
	&0 \leqslant \int_{0}^{b}\liminf\limits_{n\rightarrow\infty}\vartheta_{n}(t)dt\leqslant\liminf\limits_{n\rightarrow\infty}\int_{0}^{b}\vartheta_{n}(t)dt\leqslant\limsup\limits_{n\rightarrow\infty}\int_{0}^{b}\vartheta_{n}(t)dt\leqslant 0,\notag\\
	\Rightarrow\quad&\int_{0}^{b}\vartheta_{n}(t)(dt)\rightarrow\vartheta\text{ as }n\rightarrow\infty
\end{align}

We have $|\vartheta_{n}|=\vartheta_{n}^{+}+\vartheta_{n}^{-}=\vartheta_{n}+2\vartheta_{n}^{-}$ and $\vartheta_{n}^{-}(t)\rightarrow 0$ for almost all $t\in T$ (see (11)). Also, from (10) we have
\[
	\gamma_{n}(t)\leqslant\vartheta_{n}(t)\text{ for almost all }t\in T,\text{ all }n\in N.
\]
and $\{\gamma_{n}\}_{n\geqslant 1}\subseteq L'(T)$ is uniformly integrable. We have
\begin{align*}
	&0 \leqslant\vartheta_{n}(t)\text{ for almost all }t\in T,\text{ all }n\in N,\\
	\Rightarrow\quad&\{\vartheta_{n}^{-}(t\}_{n\geqslant 1}\subseteq L'(T)\text{ is uniformly integrable}.
\end{align*}

Applying the extended dominated convergence theorem (see, for example, Gasinski-Papageorgiou [7], p. 901), we have
\begin{align*}
	&\int_{0}^{b}\vartheta_{n}^{-}(t)dt\rightarrow 0,\\
	\Rightarrow\quad\vartheta_{n}&\rightarrow 0\text{ in }L'(T)\text{ (see (12))}
\end{align*}

So, by passing to a subsequence if necessary, we may assume that
\begin{align*}
	&\vartheta_{n}(t)\rightarrow 0\text{ for almost all }t\in T,\\
	\Rightarrow\quad\langle v_{n}(t),&y_{n}(t)-y(t)\rangle\rightarrow 0\text{ for almost all }t\in T
\end{align*}

Since $v_{n}(t)\in A(t,y_{n})$ for almost all $t\in T$, all $n\in N$, on account of the pseudomonotonicity of $A(t,\cdot)$ (see hypothesis H(A)($ii$), we have 
\[
	v(t)=A(t,y(t))=A_{\epsilon}(t,u(t))\text{ for almost all }t\in T
\]
and $v_{n}(t)\xrightarrow[]{w}$ in $X^{*}$, $\langle v_{n}(t),y_{n}(t)\rangle\rightarrow\langle v(t),y(t)\rangle$ for almost all $t\in T$.

From the dominated convergence theorem, we have
\begin{align*}
	v_{n}\xrightarrow[]{w}v\text{ in }L^{p'}(T,X^{*}),((v_{n},y_{n}))\rightarrow((v,y)),v\in\hat{A}(y),\\
	\Rightarrow\quad v_{n}\xrightarrow[]{w}v\text{ in }L^{p'}(T,V^{*}),((v_{n},y_{n}))\rightarrow((v,y))_{*},v\in\hat{A}_{\epsilon}(u).
\end{align*}
Finally, using Proposition 2.23, p. 43, of Hu-Papageorgiou [9], we see easily that $\hat{A}_{\epsilon}(\cdot)$ is usc from finite dimensional subspace of $L^{p}(T,V^{*})$ into $L^{p'}(T,V*)_{w}$.

Therefore we conclude that $\hat{A}_{\epsilon}$ is L-pseudomonotone

We consider the following auxiliary approximate periodic problem:

\begin{equation}
\left\{
\begin{array}{lr}
	u'(t)+A_{\epsilon}(t,u(t))\ni 0\text{ for almost all }t\in T,\\
	u(0)=u(b).
\end{array}
\right\}
\end{equation}
\begin{proposition}
	If hypotheses H(B),H(A) hold and $\epsilon > 0$, then problem (13) has a solution $u_{\epsilon}\in W^{per}_{p}((0,b),V^{*})$
\end{proposition}
\begin{proof}
	We write (13) as the following abstract operator inclusion
\begin{equation}
	L(u)+\hat{A}_{\epsilon}(u)\ni 0.
\end{equation}
\end{proof}

Let $v\in\hat{A}_{\epsilon}(u)$. We have
\[
	((v,u))_{*}=((v,(\epsilon J+B)^{-1}u))
\]

Let $y=(\epsilon J+B)^{-1}u$. Then $v\in\hat{A}(y)$ and so using hypothesis H(A)(iv) we have
\begin{align}
	&((v,y))=\int_{0}^{b}\langle v(t),y(t)\rangle dt\geqslant c_3||y||^{p}_{L^{p}(T,X)}-||c_4||_1,\notag\\
	\Rightarrow\quad&((v,u))_{*}\geqslant c_5||u||^{p}_{L^{p}(T,V^{*})}-||c_4||_1\text{ for some } c_5 > 0
\end{align}
(recall that $|\cdot|_{*}$ and $||\cdot||_{*}$ are equivalent norms on $X^{*}$). It follows that $\hat{A}_{\epsilon}(\cdot)$ is coercive. Clearly it is bound (see hypothesis H(A)(iii)). Also from Proposition 5 we know that $\hat{A}_{\epsilon}(\cdot)$ is L-pseudomonotone. Since $L(\cdot)$ is maximal monotone, we can use Theorem 4 and find $u_{\epsilon}\in W^{per}_{p}((0,b),V^{*})=D(L)$ such that it solves (14). Evidently this is a solution for (13).

Next we will let $\epsilon\downarrow 0$ to produce a solution for problem (1).
\begin{theorem}
	If hypotheses H(B),H(A) hold, then problem (1) has a solution for $y\in L^{p}(T,X)$ which satisfies $(By)'\in L^{p'}(T,X^{*}$.
\end{theorem}
\begin{proof}
	For each $\epsilon >0$, let $u_{\epsilon}\in W^{per}_{p}((0,b),V*)$ be a solution for the approximate problem (13). (see Proposition 6). We have
\end{proof}
\begin{equation}
	\left\{
	\begin{array}{lr}
		u'_{\epsilon}(t)+A_{\epsilon}(t,u_{\epsilon}(t))\ni 0\text{ for almost all }t\in T,\\
		u_{\epsilon}(0)=u_{\epsilon}(b)
	\end{array}
	\right\}
\end{equation}

We take inner product in $V^{*}$ with $u_{\epsilon}(t)$. Then
\[
	\frac{1}{2}\frac{d}{dt}|u'_{\epsilon}(t)|_{*}^{2}+(v_{\epsilon}(t),u_{\epsilon}(t))_{*}=0\text{ for almost all }t\in T,
\]
with $v_{\epsilon}\in L^{p'}(T,V^{*}),v_{\epsilon}(t)\in A_{\epsilon}(t,u_{\epsilon}(t))$ for almost all $t\in T$. Integrating on $T$ and using (15) and the periodic conditions, we obtain
\begin{align}
	&c_5||u_{\epsilon}||_{L^{p}(T,V^{*}}\leqslant||c_4||_1,\notag\\
	\Rightarrow\quad\{u_{\epsilon}\}&_{\epsilon>0}\subseteq L^{p}(T,V^{*})\text{ is bounded.}
\end{align}

We set $y_{\epsilon}(t)=(\epsilon J+B)^{-1}u_{\epsilon}(t)$. Then
\begin{align}
	&||y_{\epsilon}(t)\leqslant||(\epsilon J+B)^{-1}||\mathcal{L}||u_{\epsilon}(t)||^{*}\notag\\
	\Rightarrow\quad\{y_{\epsilon}\}&_{\epsilon\in(0,1]}\subseteq L^{p}(T,V^{*})\text{ is bounded (see (17))}
\end{align}

On account of hypothesis H(A)(iii), we have
\begin{equation}
	|A_{\epsilon}(t,u_{\epsilon}(t))|\leqslant c_1(t)+c_2||y_{\epsilon}(t)||^{p-1}\text{ for almost all }t\in T
\end{equation}

Then from (16),(18) and (19) it follows that
\[
	\{u'_{\epsilon}\}_{\epsilon\in(0,1]}\subseteq L^{p'}(T,V^{*})\text{ is bounded}
\]

This together with (17) imply that 
\begin{equation}
	\{u_{\epsilon}\}_{\epsilon\in(0,1]}\subseteq W^{1,p'}((0,b),V^{*})\text{ is bounded (recall $1<p'\leqslant 2\leqslant p$)}
\end{equation}

Now let $\epsilon_{n}=\frac{1}{n},u_{n}=u_{\epsilon_{n}},y_{n}=y_{\epsilon_{n}},v_{n}=v_{\epsilon_{n}}$ for all $n\in N$. Note that
\[
	[(\frac{1}{n}J+B)y_{n}(t)]'\in L^{p'}(T,X^{*}
\]

We have
\begin{equation}
	\left\{
	\begin{array}{lr}
		((\frac{1}{n}J+B))y_{n}(t)'+v_{n}(t)=0\text{ for almost all }t\in T,\\
		v_{n}(t)\in A(t,y_{n}(t))\text{ for almost all }t\in T,\\
		u_{n}(0)=n_{n}(b).
	\end{array}
	\right\}
\end{equation}

Note that
\begin{equation}
	y_{n}(0)=(\epsilon J+B)^{-1}u_{n}(0)=(\epsilon J+B)^{-1}u_{n}(b)=y_{n}(b)\text{ for all }n\in\mathbb{N}\text{ (see (21))}
\end{equation}

Also, on account of (18), (20) and (21), we may assume that
\begin{equation}
	y_{n}\xrightarrow[]{w}y\text{ in }L^{p}(T,X),u_{n}\xrightarrow[]{w}u\text{ in }W^{1,p'}((0,b),V^{*}),v_{n}\rightarrow v\text{ in }L^{p'}(T,X^{*})
\end{equation}

We know that $W^{1,p'}((0,b),V^{*})\hookrightarrow C(T,V^{*})$ continuously. Hence from (17) at least for a subsequence, we have
\begin{align}
	u_{n}\xrightarrow[]{w}&\text{ in }C(T,V^{*}),\notag\\
	\Rightarrow\quad y_{n}(t)\xrightarrow[]{w}y&(t)\text{ in }X\text{ for all }t\in T,\\
	\Rightarrow\quad B(y(0))=&B(y(b))\text{ (see (22))}.
\end{align}

On the first equation in (21) we act with $(y_{n}-y)(t)$ and then integrate over $T$. We obtain
\begin{equation}
	(((\left[\frac{1}{n}J+B\right]y_{n})',y_{n}-y))+((v_{n},y_{n}-y))=0\text{ for all }n\in \mathbb{N}	
\end{equation}

We have
\begin{align}
	(((&\left[\frac{1}{n}J+B\right]y_{n})',y_{n}-y))\notag\\
	=(((\left[\frac{1}{n}J+B\right](y_{p}y)&)',y_{n}-y))+(((\frac{1}{n}J+B]y)',y_{n}-y))
\end{align}

Note that
\begin{equation}
	(((\left[\frac{1}{n}J+B\right]y)',y_{n}-y))\rightarrow 0\text{ as }n\rightarrow\infty\text{ (see (23))}
\end{equation}

Also, we have
\begin{align}
	&(((\left[\frac{1}{n}J+B\right](y_{n}-y))',y_{n}-y))\notag\\
	=&\int_{0}^{b}\langle\frac{1}{n}(J(y_{n}-y))',y_{n}-y\rangle dt+\int_{0}^{b}\langle(B(y_{n}-y))',y_{n}-y\rangle dt\notag\\
	=&\int_{0}^{b}\frac{1}{n}(y'_{n}-y',y_{p}-y)_{x}dt+\frac{1}{2}\int_{0}^{b}\frac{d}{dt}\langle B(y_{n}-y),y_{n}-y\rangle dt\notag\\
	\text{ recall that }J&(\cdot)\text{ is the Reisz map for $X$ and see hypothesis H(B)}\notag\\
	=&\frac{1}{n}[||(y_{p}-y)(b)||-||(y_{n}-y)(0)||]+\frac{1}{2}[\langle B(y_{n}-y)(b),(y_{n}-y)(b)\rangle - \notag\\ &\langle B(y_{n}-y)(0),(y_{n}-y)(0b)\rangle]\notag\\
	=&0\text{ for all $n\in\mathbb{N}$ (see (22), (24))}
\end{align}

So, if we return to (27) and use (28), (29) we obtain
\begin{equation}
	\lim\limits_{n\rightarrow\infty}(((\left[\frac{1}{n}J+B\right]y_{n})',y_{n}-y))=0.
\end{equation}

If we use (30) in (26) we have
\[
	\lim\limits_{n\rightarrow\infty}((v_{n},y_{n}-y))=0
\]
Invoking Proposition 5, we have
\[
	v\in\hat{A}(y)\text{ and }((v_{n},y_{n}))\rightarrow((v,y))
\]

Thus from (21) in the limit as $n\rightarrow\infty$ we obtain
\[
\left\{
	\begin{array}{lr}
		\frac{d}{dt}(By(T))+A(t,y(t))\ni 0\text{ for almost all }t\in T,\\
		B(y(0))=B(y(b)).	
	\end{array}
\right\}
\]

Therefore $y\in L^{p}(T,X)$ is a solution of (1) with $(By)'\in L^{p'}(T,X^{*}$
\section{An Example}

Let $T=[0,b]$ and let $\Omega \subseteq \mathbb{R^{N}}$ be a bounded domain with a $C^2$ -\textit{ bounday } $\partial\Omega$. We consider the following initial boundary value problem
\begin{equation}
	\left\{
	\begin{array}{lr}
		\frac{d}{dt}(m(z)u)-div(a(t,z)Du)+\sum_{k=1}^{N}(\sin u)D_{k}u+\partial g(u)\ni 0\text{ in }T\times\Omega ,\\
		u|_{T\times\partial\Omega}=0,m(z)u(z,0)=m(z)u(z,b)\text{ for almost all }z\in\Omega .
	\end{array}
	\right\}
\end{equation}

We impose the following conditions on the data of (31).

$H(m): m\in L^{\frac{N}{2}}(\Omega)$ if $N>2,m\in L^{r}(\Omega)$ with $r>1$ if $N=2$ and $m\in L'(\Omega)$ if $N=1,m(z)\geqslant 0$ for almost all $z\in\Omega,m\not\equiv 0$

$H(a): a\in L^{\infty}(T\times\Omega)$ and $a(t,z)\geqslant a_0>0$ for almost all $(t,z)\in T\times\Omega$.

$H(g):g:\mathbb{R}\rightarrow\mathbb{R}$ is a continuous convex function and its subdifferential $\partial g(x)$ satisfies
\[
	|\partial g(x)|\leqslant\hat{c}[1+|x|^{p-1}]\text{ for all }x\in\mathbb{R},\text{ some }\hat{c}>0,2\leqslant p<\infty.
\]
\begin{remark}
	For a continuous convex function $g(\cdot)$, we know that $\partial g(x)\not=\varnothing$ for all $x\in\mathbb{R}$ (see Gasinski-Papageorgiou [7], p.527).
\end{remark}

We introduce the following multifunction
\[
	N_{g}(u)=\{v\in L^{p'}(\Omega):v(z)\in\partial g(u(z))\text{ for almost all }z\in\Omega\}
\]
for all $u\in H_{0}^{1}(\Omega)$. Evidently $N_{g}(\cdot)$ is maximal monotone.

In this case the evolution triple consists of the following Hilbert spaces
\[
	X=H_{0}^{1}(\Omega),H=L^{2}(\Omega),X^{*}=H^{-1}(\Omega).
\]

We know that $X\rightarrow H$ compactly (by Sobolev embedding theorem).

Let $A_{1}:T\times X\rightarrow X^{*}$ be the nonlinear map defined by 
\begin{align*}
	\langle A_{1}(t,u),h\rangle =\int_{\Omega}a(t,z)(Du,Dh)_{\mathbb{R}^{\mathbb{N}}}dz+\int_{\Omega}\sin u(\sum_{k=1}^{N}D_{k}u)hdz\\
	\text{ for all }u,h\in X=H_{0}^{1}(\Omega)
\end{align*}

Then $t\rightarrow A_{1}(t,u)$ is measurable, while $u\rightarrow A_{1}(t,u)$ is pseudomonotone(see, for example, Zeidler [15], p. 591). We set
\[
	A(t,u)=A_{1}(t,u)+N_{g}(u)
\]

Then $A(t,u)$ satisfies hypotheses H(A) (see H(a) and H(g)).

In addition we let $B\in\mathcal{L}(X,X^{*})$ be defined by
\[
	Bu(\cdot)=m(\cdot)u(\cdot)\text{ for all }u\in X=H_{0}^{1}(\Omega).
\]

Clearly $B(\cdot)$ satisfies H(B).1

We can rewrite the problem (31) as the following abstract implicit evolution inclusion
\[
\left\{
\begin{array}{lr}
	\frac{d}{dt}(Bu(t))+A(t,u(t))\ni 0\text{ for almost all }t\in T,\\
	B(u(0))=B(u(b))
\end{array}
\right\}
\]

We can apply Theorem 7 and have:

\begin{proposition}
	If hypotheses H(m),H(a),H(g) hold, then problem (31) admits a solution $u\in L^{P}(T,H_{0}^{1}(\Omega))$ with
	\[
		(Bu)'\in L^{p'}(T,H^{-1}(\Omega)).
	\]
\end{proposition}
\begin{remark}
	Using the method of this paper we can also treat antiperiodic problems (see Gasinski-Papageorgiou [8]) and applications to distributed parameter control systems (see Papageorgiou-Radulescu-Repovs [13]).
\end{remark}

\begin{thebibliography}{15}
	\bibitem{1}
	K. Andrews, K. Kuttler, M. Schillor,
	\textit{Second order evolution equations with dynamic boundary conditions,}
	J. Math. Anal. 197 (1996), 781-795
	\bibitem{2}
	V. Barbu, 
	\textit{Nonlinear Semigroups and Differential Equations in Banach Spaces}, Noordhoff, Leyden, The Netherlands (1976)
	\bibitem{3}
	V. Barbu, A. Favini, \textit{Existence for an implicit differential equation}, Nonlinear Anal. 32 (1998), 33-40
	\bibitem{4}
	V. Barbu, A. Favini, \textit{Existence for implicit differential equations in Banach spaces}, Atti Accad. Naz. Lincei Cl. Sci. Fiz. Mat. Natur. Rend. Mat. Appl. 3 (1992), 203-215
	\bibitem{5}
	E. DiBenedetto, R. Showalter, \textit{A pseudo-parabolic variational inequality and Stefan problem}, Nonlin. Anal. 6 (1982), 279-291
	\bibitem{6}
	A. Favini, A. Yagi, \textit{Multivalued linear operators and degenerate evolution equations}, Annali di Mat. Pura Appl. 163 (1993), 353-384
	\bibitem{7}
	L. Gasinski, N. S. Papageorgiou, \textit{Nonlinear Analysis}, Chapman \& Hall/CRC, Boca Raton, FL (2006).
	\bibitem{8}
	L. Gasinski, N. S. Papageorgiou, \textit{Anti-periodic solutions for nonlinear evolution inclusions} J. Evolution Equations
	\bibitem{9}
	S. Hu, N. S. Papageorgiou, \textit{Handbook of Multivalued Analysis. Volume I: Theory Kluwer Academic Publisher}, Dordrecht, The Netherlands (1997)
	\bibitem{10}
	J. I. Lions, \textit{Quelques Methodes de Resolution des Problems aux Limites Non-Lineaires}, Dunod, Paris (1969)
	\bibitem{11}
	Z. Liu, \textit{Existence for implicit differential equations with monotone perturbations}, Israel J. Math. 129 (2002), 363-372
	\bibitem{12}
	N. S. Papageorgiou, F. Papalini, F. Renzacci \textit{Existence of solutions and periodic solutions for nonlinear evolution inclusions} Rend. Cir. Mat. Parlermo 48 (1999), 341-364
	\bibitem{13}
	N. S. Papageorgiou, V. D. Radulescu, D. Repovs, \textit{Sensitivity analysis for optimal control problems governed by nonlinear evolution inclusions}, Adv. Nonlin. Anal 6 (2017) 199-225
	\bibitem{14}
	R. Showalter, \textit{Monotone Operators in Banach Space and Nonlinear Partial Differential Equations}, Math. Surveys and Monographs, Vol. 49, American Math. Soc., providence, RI (1997)
	\bibitem{15}
	E. Zeidler, \textit{Nonlinear Functional Analysis and its Applications II/B}, Springer, New York (1990)
\end{thebibliography}
\begin{obeylines}
\textsc{
National Technical University, Department of Mathematics, Zografou Campus, Athens 15780, Greece}
\textit{E-mail address: }\textbf{npapg@@math.ntua.gr}


\textsc{
University of Craiova, Department of Mathematics, Street A.I.Cuza 13, 200585 Craiova, Romania, and Institute of Mathematics "Simion Stoilow" of the Romanian Academy, P.O. Box 1-764, 014700 Bucharest, Romania}
\textit{E-mail address: }\textbf{vicentiu.radulescu@imar.ro}
\textsc{
Faculty of Education and Faculty of Mathematics and Physics, University of Ljublijiana, Karadeljeva Ploscad 16, SI-1000 Ljubljana, SLOVENIA}
\textit{E-mail address: }\textbf{dusan.repovs@guest.arnes.si}
\end{obeylines}
\include{resources/tema}
\end{document}